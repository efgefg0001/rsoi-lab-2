package org.bitbucket.efgefg0001.oauth2.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import org.bitbucket.efgefg0001.oauth2.model.enums.GrantType;
import org.bitbucket.efgefg0001.oauth2.model.request.LoginRequest;
import org.bitbucket.efgefg0001.oauth2.service.UserServiсe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class LoginController {

    private static final ObjectMapper OM = new ObjectMapper();

    private static final String USERNAME_ERROR_KEY = "usernameError";
    private static final String PASSWORD_ERROR_KEY = "passwordError";
    private static final String USER_KEY = "user";
    private static final String TOKEN_KEY = "token";

    private static final String USERNAME_ERROR_DOES_NOT_EXISTS = "Пользователь с таким идентификатором не зарегистрирован";
    private static final String PASSWORD_ERROR_MSG = "Неправильный пароль";

    private AuthorizationRestController authorizationRestController;
    private UserServiсe userServiсe;

    @Autowired
    public LoginController(UserServiсe userServiсe, AuthorizationRestController authorizationRestController) {
        this.userServiсe = userServiсe;
        this.authorizationRestController = authorizationRestController;
    }

    @GetMapping("/login")
    public String loginForm(Model model) {
        model.addAttribute(USER_KEY, new LoginRequest());
        return "login";
    }

    @PostMapping("/login")
    public String loginSubmit(@ModelAttribute LoginRequest request, Model model) throws Exception {

        val username = request.getUsername();
        val exists = userServiсe.exists(username);
        if (!exists) {
            model.addAttribute(USER_KEY, request);
            model.addAttribute(USERNAME_ERROR_KEY, USERNAME_ERROR_DOES_NOT_EXISTS);
            return "login";
        }

        val userFromDb = userServiсe.get(username);

        val sentPassword = request.getPassword();
        val passwordFromDb = userFromDb.getPassword();
        if (!sentPassword.equals(passwordFromDb)) {
            model.addAttribute(USER_KEY, request);
            model.addAttribute(PASSWORD_ERROR_KEY, PASSWORD_ERROR_MSG);
            return "login";
        }

        val token = authorizationRestController.authorize(
                GrantType.Str.PASSWORD,
                userFromDb.getClientId(),
                userFromDb.getClientSecret(),
                username, sentPassword, ""
        );

        val tokenAsJsonNode = OM.convertValue(token, JsonNode.class);

        model.addAttribute(TOKEN_KEY, tokenAsJsonNode);

        return "login-success";
    }
}
