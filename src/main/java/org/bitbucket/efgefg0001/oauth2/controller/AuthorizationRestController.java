package org.bitbucket.efgefg0001.oauth2.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import org.bitbucket.efgefg0001.oauth2.Utils;
import org.bitbucket.efgefg0001.oauth2.exception.InvalidRequestException;
import org.bitbucket.efgefg0001.oauth2.model.enums.GrantType;
import org.bitbucket.efgefg0001.oauth2.model.response.TokenResponse;
import org.bitbucket.efgefg0001.oauth2.service.TokenService;
import org.bitbucket.efgefg0001.oauth2.service.UserServiсe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AuthorizationRestController {

    private static final String PASSWORD_ERROR_MSG = "password is invalid";
    private static final String USER_ERROR_MSG = "user with username '%S' is not registered";
    private static final String CLIENT_ID_ERROR_MSG = "client_id is invalid";
    private static final String CLIENT_SECRET_ERROR_MSG = "client_secret is invalid";

    private static final String ERROR_KEY = "error";

    private static final Long EXPIRES_IN = 6000L;
    private static final ObjectMapper OM = new ObjectMapper();

    private UserServiсe userServiсe;
    private TokenService tokenService;
    private Utils utils;

    @Autowired
    public AuthorizationRestController(UserServiсe userServiсe, TokenService tokenService, Utils utils) {
        this.userServiсe = userServiсe;
        this.tokenService = tokenService;
        this.utils = utils;
    }

    @PostMapping(value = "/oauth2/token", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ResponseBody
    public TokenResponse authorize(
            @RequestParam("grant_type") String grantType,
            @RequestParam("client_id") String clientId,
            @RequestParam("client_secret") String clientSecret,
            @RequestParam(value = "username", required = false) String username,
            @RequestParam(value = "password", required = false) String password,
            @RequestParam(value = "refresh_token", required = false) String refreshToken
    ) throws Exception {
        TokenResponse response = new TokenResponse();
        switch (grantType) {
            case GrantType.Str.PASSWORD:
                response = handleGrantTypePassword(username, password, clientId, clientSecret);
                break;
            case GrantType.Str.REFRESH_TOKEN:
                response = handleGrantTypeRefreshToken(clientId, clientSecret, refreshToken);
                break;
            default:
                throw new InvalidRequestException("Unknown grant_type '" + grantType + "'");
        }
        return response;
    }

    public TokenResponse handleGrantTypeRefreshToken(String clientId, String clientSecret, String refreshToken) throws Exception {

        val username = tokenService.getUsernameByRefreshToken(refreshToken);
        val userFromDb = userServiсe.get(username);

        val clientIdFromDb = userFromDb.getClientId();
        if (!clientId.equals(clientIdFromDb)) {
            throw new InvalidRequestException(CLIENT_ID_ERROR_MSG);
        }

        val clientSecretFromDb = userFromDb.getClientSecret();
        if (!clientSecret.equals(clientSecretFromDb)) {
            throw new InvalidRequestException(CLIENT_SECRET_ERROR_MSG);
        }

        return tokenService.createToken(username, EXPIRES_IN);
    }

    public TokenResponse handleGrantTypePassword(String username, String password, String clientId, String clientSecret) throws Exception {
        val exists = userServiсe.exists(username);
        if (!exists) {
            val msg = String.format(USER_ERROR_MSG, username);
            throw new InvalidRequestException(msg);
        }

        val userFromDb = userServiсe.get(username);

        val passwordFromDb = userFromDb.getPassword();
        if (!password.equals(passwordFromDb)) {
            throw new InvalidRequestException(PASSWORD_ERROR_MSG);
        }

        val clientIdFromDb = userFromDb.getClientId();
        if (!clientId.equals(clientIdFromDb)) {
            throw new InvalidRequestException(CLIENT_ID_ERROR_MSG);
        }

        val clientSecretFromDb = userFromDb.getClientSecret();
        if (!clientSecret.equals(clientSecretFromDb)) {
            throw new InvalidRequestException(CLIENT_SECRET_ERROR_MSG);
        }

        return tokenService.createToken(username, EXPIRES_IN);
    }

}
