package org.bitbucket.efgefg0001.oauth2.controller;

import lombok.val;
import org.bitbucket.efgefg0001.oauth2.exception.UnathorizedException;
import org.bitbucket.efgefg0001.oauth2.model.response.BaseResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice
@RestController
public class GlobalExceptionHandler {

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(UnathorizedException.class)
    public BaseResponse handleUnathorizedException(UnathorizedException e) {
        val msg = e.getMessage();
        val resp = new BaseResponse();
        resp.setErrorMessage(msg);
        return resp;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(Exception.class)
    public BaseResponse handleException(Exception e) {
        val msg = e.getMessage();
        val resp = new BaseResponse();
        resp.setErrorMessage(msg);
        return resp;
    }

}
