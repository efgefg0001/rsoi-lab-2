package org.bitbucket.efgefg0001.oauth2.controller;

import lombok.val;
import org.bitbucket.efgefg0001.oauth2.Utils;
import org.bitbucket.efgefg0001.oauth2.model.entity.UserData;
import org.bitbucket.efgefg0001.oauth2.model.request.RegisterRequest;
import org.bitbucket.efgefg0001.oauth2.model.request.SaveUserDataRequest;
import org.bitbucket.efgefg0001.oauth2.model.request.SaveUserRequest;
import org.bitbucket.efgefg0001.oauth2.service.UserServiсe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class RegisterController {

    private static final String USERNAME_ERROR_KEY = "usernameError";

    private static final String USERNAME_ERROR_EXISTS = "Пользователь с таким идентификатором пользователя уже зарегистрирован";

    private UserServiсe userServiсe;
    private Utils utils;

    @Autowired
    public RegisterController(UserServiсe userServiсe, Utils utils) {
        this.userServiсe = userServiсe;
        this.utils = utils;
    }

    @GetMapping("/register")
    public String registerForm(Model model) {
        model.addAttribute("user", new RegisterRequest());
        return "register";
    }

    @PostMapping("/register")
    public String registerSubmit(@ModelAttribute RegisterRequest request, Model model) {
        val username = request.getUsername();
        val exists = userServiсe.exists(username);
        if (exists) {
            model.addAttribute("user", request);
            model.addAttribute(USERNAME_ERROR_KEY, USERNAME_ERROR_EXISTS);
            return "register";
        }

        val saveUserRequest = utils.copy(request, SaveUserRequest.class);
        val saveUserDataRequest = utils.copy(request, SaveUserDataRequest.class);
        userServiсe.save(saveUserRequest, saveUserDataRequest);

        return "register-success";
    }
}
