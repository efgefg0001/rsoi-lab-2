package org.bitbucket.efgefg0001.oauth2.controller;

import com.fasterxml.jackson.databind.JsonNode;
import org.bitbucket.efgefg0001.oauth2.model.request.BookRequest;
import org.bitbucket.efgefg0001.oauth2.model.request.UpdateBookRequest;
import org.bitbucket.efgefg0001.oauth2.model.response.ResultResponse;
import org.bitbucket.efgefg0001.oauth2.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class BookController {

    private BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @PostMapping("/protected/publHouse/{publHouseId}/book")
    public ResultResponse create(@PathVariable("publHouseId") Long id, @RequestBody BookRequest request) {
        return bookService.create(request, id);
    }

    @GetMapping("/protected/publHouse/{pId}/book/{bId}")
    public JsonNode get(@PathVariable("bId") Long id, @PathVariable("pId") Long pId) {
        return bookService.getById(id, pId);
    }


    @DeleteMapping("/protected/publHouse/{pId}/book/{bId}")
    public ResultResponse delete(@PathVariable("bId") Long id, @PathVariable("pId") Long pId) {
        return bookService.deleteById(id, pId);
    }

    @PatchMapping("/protected/publHouse/{pId}/book/{bId}")
    public JsonNode update(@PathVariable("pId") Long pId, @PathVariable("bId") Long bId, @RequestBody UpdateBookRequest request) {
        return bookService.update(bId, pId, request);
    }

}
