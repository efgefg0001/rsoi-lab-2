package org.bitbucket.efgefg0001.oauth2.controller;

import lombok.val;
import org.bitbucket.efgefg0001.oauth2.model.response.UserInfoResponse;
import org.bitbucket.efgefg0001.oauth2.service.TokenService;
import org.bitbucket.efgefg0001.oauth2.service.UserServiсe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserInfoController {

    private UserServiсe userServiсe;
    private TokenService tokenService;

    @Autowired
    public UserInfoController(UserServiсe userServiсe, TokenService tokenService) {
        this.userServiсe = userServiсe;
        this.tokenService = tokenService;
    }

    @GetMapping("/me")
    public UserInfoResponse getMe(@RequestHeader(value = "Authorization") String authVal) {
        val accessToken = getAccessToken(authVal);
        val username = tokenService.getUsernameByAccessToken(accessToken);
        return userServiсe.getUserInfo(username);
    }

    private String getAccessToken(String authVal) {
        val words = authVal.split("\\s");
        return words[1];
    }
}
