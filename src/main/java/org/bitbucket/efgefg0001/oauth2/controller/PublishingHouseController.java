package org.bitbucket.efgefg0001.oauth2.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.bitbucket.efgefg0001.oauth2.model.request.PublHouseRequest;
import org.bitbucket.efgefg0001.oauth2.model.response.ResultResponse;
import org.bitbucket.efgefg0001.oauth2.service.PublishingHouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PublishingHouseController {

    private static final ObjectMapper OM = new ObjectMapper();

    private PublishingHouseService publishingHouseService;

    @Autowired
    public PublishingHouseController(PublishingHouseService publishingHouseService) {
        this.publishingHouseService = publishingHouseService;
    }

    @PostMapping("/protected/publHouse")
    public ResultResponse create(@RequestBody PublHouseRequest request) {
        return publishingHouseService.create(request);
    }

    @GetMapping("/protected/publHouse/{id}")
    public JsonNode create(@PathVariable("id") Long id) {
        return publishingHouseService.getById(id);
    }

    @PatchMapping("/protected/publHouse/{id}")
    public JsonNode update(@PathVariable("id") Long id, @RequestBody PublHouseRequest request) {
        return publishingHouseService.update(id, request);
    }

    @GetMapping("/protected/publHouse")
    public List<JsonNode> getAll(@RequestParam("page") Integer page, @RequestParam("size") Integer size) {
        return publishingHouseService.getWithPaging(page, size);
    }

}
