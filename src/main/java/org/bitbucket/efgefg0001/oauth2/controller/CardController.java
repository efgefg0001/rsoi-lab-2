package org.bitbucket.efgefg0001.oauth2.controller;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.val;
import org.bitbucket.efgefg0001.oauth2.model.request.CreateCardRequest;
import org.bitbucket.efgefg0001.oauth2.model.response.CardResponse;
import org.bitbucket.efgefg0001.oauth2.model.response.ResultResponse;
import org.bitbucket.efgefg0001.oauth2.service.CardService;
import org.bitbucket.efgefg0001.oauth2.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CardController {

    @Autowired
    private CardService cardService;

    @Autowired
    private TokenService tokenService;

    @PostMapping("/protected/card")
    public ResultResponse create(@RequestHeader(value = "Authorization") String authVal, @RequestBody CreateCardRequest request) {
        val accessToken = getAccessToken(authVal);
        val username = tokenService.getUsernameByAccessToken(accessToken);
        return cardService.create(username, request);
    }

    @GetMapping("/protected/card")
    public List<CardResponse> get(@RequestHeader(value = "Authorization") String authVal, @RequestParam("page") Integer page, @RequestParam("size") Integer size) {
        val accessToken = getAccessToken(authVal);
        val username = tokenService.getUsernameByAccessToken(accessToken);
        return cardService.getAll(username, page, size);
    }

    private String getAccessToken(String authVal) {
        val words = authVal.split("\\s");
        return words[1];
    }
}
