package org.bitbucket.efgefg0001.oauth2.exception;

public class UnathorizedException extends Exception {
    public UnathorizedException(String msg) {
        super(msg);
    }
}
