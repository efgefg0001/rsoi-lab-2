package org.bitbucket.efgefg0001.oauth2.exception;

public class InvalidRequestException extends Exception {
    public InvalidRequestException(String msg) {
        super(msg);
    }
}
