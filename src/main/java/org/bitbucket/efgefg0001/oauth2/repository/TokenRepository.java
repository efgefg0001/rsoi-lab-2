package org.bitbucket.efgefg0001.oauth2.repository;

import org.bitbucket.efgefg0001.oauth2.model.entity.Token;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TokenRepository extends CrudRepository<Token, Long> {
    List<Token> findByAccessToken(String accessToken);
    List<Token> findByRefreshToken(String refreshToken);
}
