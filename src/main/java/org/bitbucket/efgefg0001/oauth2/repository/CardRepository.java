package org.bitbucket.efgefg0001.oauth2.repository;

import org.bitbucket.efgefg0001.oauth2.model.entity.Card;
import org.bitbucket.efgefg0001.oauth2.model.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CardRepository extends CrudRepository<Card, Long> {
    List<Card> findByUser(User user);
}
