package org.bitbucket.efgefg0001.oauth2.repository;

import org.bitbucket.efgefg0001.oauth2.model.entity.User;
import org.bitbucket.efgefg0001.oauth2.model.entity.UserData;
import org.springframework.data.repository.CrudRepository;

public interface UserDataRepository extends CrudRepository<UserData, Long> {
    UserData findByUser(User user);
}
