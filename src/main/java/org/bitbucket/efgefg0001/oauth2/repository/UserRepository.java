package org.bitbucket.efgefg0001.oauth2.repository;

import org.bitbucket.efgefg0001.oauth2.model.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {
    List<User> findByUsername(String username);
}
