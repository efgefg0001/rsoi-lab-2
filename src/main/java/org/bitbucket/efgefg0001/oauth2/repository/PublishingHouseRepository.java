package org.bitbucket.efgefg0001.oauth2.repository;

import org.bitbucket.efgefg0001.oauth2.model.entity.PublishingHouse;
import org.springframework.data.repository.CrudRepository;

public interface PublishingHouseRepository extends CrudRepository<PublishingHouse, Long> {
}
