package org.bitbucket.efgefg0001.oauth2.interceptor;

import lombok.val;
import org.bitbucket.efgefg0001.oauth2.exception.UnathorizedException;
import org.bitbucket.efgefg0001.oauth2.service.TokenService;
import org.bitbucket.efgefg0001.oauth2.service.TokenServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;

@Component
public class OAuth2Interceptor extends HandlerInterceptorAdapter {

    private final static String AUTH_HEADER_NAME = "authorization";
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private TokenService tokenService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        val headers = request.getHeaderNames();
        List<String> headersAsList = Collections.list(headers);
        logger.info("headers = {}", headersAsList);
        String authVal = request.getHeader(AUTH_HEADER_NAME);
        val authValAsArr = authVal.split("\\s");

        val tokenType = authValAsArr[0];
        val accessToken = authValAsArr[1];

        if (!tokenService.tokenExists(accessToken))
            throw new UnathorizedException("access token is invalid");

        if (tokenService.tokenExpired(accessToken))
            throw new UnathorizedException("access token expired");

        return true;
    }
}
