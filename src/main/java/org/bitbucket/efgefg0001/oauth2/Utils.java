package org.bitbucket.efgefg0001.oauth2;

import jodd.bean.BeanCopy;
import lombok.SneakyThrows;
import lombok.val;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.UUID;

@Component
public class Utils {
    public String createUUID() {
        val uuid = UUID.randomUUID();
        return uuid.toString();
    }

    @SneakyThrows
    public <T> T copy(Object source, Class<T> clazz) {
        T dest = clazz.newInstance();
        BeanCopy beanCopy = new BeanCopy(source, dest);
        beanCopy.copy();
        return dest;
    }

    public Timestamp getNowTimestamp() {
        val now = System.currentTimeMillis();
        return new Timestamp(now);
    }
}
