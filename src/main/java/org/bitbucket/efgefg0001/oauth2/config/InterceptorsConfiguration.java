package org.bitbucket.efgefg0001.oauth2.config;

import org.bitbucket.efgefg0001.oauth2.interceptor.OAuth2Interceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class InterceptorsConfiguration extends WebMvcConfigurerAdapter {

    private final static String PROTECTED_PATH_PATTERNS = "/protected/**";
    private final static String ME_PATTERN = "/me";

    @Autowired
    private OAuth2Interceptor oAuth2Interceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(oAuth2Interceptor)
                .addPathPatterns(PROTECTED_PATH_PATTERNS, ME_PATTERN);
    }
}
