package org.bitbucket.efgefg0001.oauth2.model.response;

import lombok.Data;

@Data
public class UserInfoResponse {
    private String firstName;
    private String lastName;
    private String email;
}
