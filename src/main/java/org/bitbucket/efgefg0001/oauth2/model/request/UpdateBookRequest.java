package org.bitbucket.efgefg0001.oauth2.model.request;

import lombok.Data;

@Data
public class UpdateBookRequest {
    private String name;
    private String authors;
    private Integer year;
    private Long phId;
}
