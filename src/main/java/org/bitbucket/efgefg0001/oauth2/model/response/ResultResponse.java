package org.bitbucket.efgefg0001.oauth2.model.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResultResponse {
    private String result;
    private Long id;

    public static final String OK_RESULT = "OK";
}
