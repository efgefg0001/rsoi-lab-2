package org.bitbucket.efgefg0001.oauth2.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TokenType {
    BEARER("bearer");
    private String value;
}
