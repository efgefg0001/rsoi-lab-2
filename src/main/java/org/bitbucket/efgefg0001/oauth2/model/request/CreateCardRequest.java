package org.bitbucket.efgefg0001.oauth2.model.request;

import lombok.Data;

@Data
public class CreateCardRequest {
    private Long bookId;
}
