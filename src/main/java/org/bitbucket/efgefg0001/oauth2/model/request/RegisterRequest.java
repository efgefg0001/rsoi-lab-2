package org.bitbucket.efgefg0001.oauth2.model.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RegisterRequest {
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
}
