package org.bitbucket.efgefg0001.oauth2.model.entity;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Data
public class UserData {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Setter(AccessLevel.NONE)
    private Long id;

    @OneToOne
    @JoinColumn(name = "registered_user")
    private User user;

    private String firstName;
    private String lastName;
    private String email;
}
