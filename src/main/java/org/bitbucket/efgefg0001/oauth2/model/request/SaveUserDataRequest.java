package org.bitbucket.efgefg0001.oauth2.model.request;

import lombok.Data;

@Data
public class SaveUserDataRequest {
    private String firstName;
    private String lastName;
    private String email;
}
