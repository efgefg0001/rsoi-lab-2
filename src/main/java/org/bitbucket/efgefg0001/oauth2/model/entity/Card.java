package org.bitbucket.efgefg0001.oauth2.model.entity;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Data
public class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Setter(AccessLevel.NONE)
    private Long id;

    @OneToOne
    @JoinColumn(name = "book")
    private Book book;

    @OneToOne
    @JoinColumn(name = "registered_user")
    private User user;

    private Timestamp date;
}
