package org.bitbucket.efgefg0001.oauth2.model.response;

import lombok.Data;
import org.bitbucket.efgefg0001.oauth2.model.entity.Book;

@Data
public class CardResponse {
    private Long cardId;
    private Long bookId;
}
