package org.bitbucket.efgefg0001.oauth2.model.request;

import lombok.Data;

@Data
public class PublHouseRequest {
    private String name;
    private Integer year;
    private String country;
    private String city;
}
