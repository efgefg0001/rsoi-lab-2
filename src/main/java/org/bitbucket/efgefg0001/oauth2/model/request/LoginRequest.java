package org.bitbucket.efgefg0001.oauth2.model.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class LoginRequest {
    private String username;
    private String password;
}
