package org.bitbucket.efgefg0001.oauth2.model.entity;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

import javax.persistence.*;

@Data
@Entity
@Table(name = "registered_user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Setter(AccessLevel.NONE)
    private Long id;

    @Column(unique = true)
    private String username;
    private String password;
    private String clientId;
    private String clientSecret;
}
