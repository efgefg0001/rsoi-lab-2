package org.bitbucket.efgefg0001.oauth2.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum GrantType {

    PASSWORD(Str.PASSWORD), REFRESH_TOKEN(Str.REFRESH_TOKEN);

    private String value;

    public static class Str {
        public static final String PASSWORD = "password";
        public static final String REFRESH_TOKEN = "refresh_token";
    }
}
