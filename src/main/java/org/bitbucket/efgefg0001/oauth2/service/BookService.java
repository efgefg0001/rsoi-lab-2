package org.bitbucket.efgefg0001.oauth2.service;

import com.fasterxml.jackson.databind.JsonNode;
import org.bitbucket.efgefg0001.oauth2.model.request.BookRequest;
import org.bitbucket.efgefg0001.oauth2.model.request.UpdateBookRequest;
import org.bitbucket.efgefg0001.oauth2.model.response.ResultResponse;

public interface BookService {

    ResultResponse create(BookRequest request, Long publHouseId);

    JsonNode getById(Long id, Long publId);

    ResultResponse deleteById(Long bId, Long pId);

    JsonNode update(Long bId, Long pId, UpdateBookRequest request);
}
