package org.bitbucket.efgefg0001.oauth2.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import org.bitbucket.efgefg0001.oauth2.Utils;
import org.bitbucket.efgefg0001.oauth2.model.entity.Book;
import org.bitbucket.efgefg0001.oauth2.model.entity.Card;
import org.bitbucket.efgefg0001.oauth2.model.request.CreateCardRequest;
import org.bitbucket.efgefg0001.oauth2.model.response.CardResponse;
import org.bitbucket.efgefg0001.oauth2.model.response.ResultResponse;
import org.bitbucket.efgefg0001.oauth2.repository.BookRepository;
import org.bitbucket.efgefg0001.oauth2.repository.CardRepository;
import org.bitbucket.efgefg0001.oauth2.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.bitbucket.efgefg0001.oauth2.model.response.ResultResponse.OK_RESULT;

@Service
public class CardServiceImpl implements CardService {

    private static final ObjectMapper OM = new ObjectMapper();

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private CardRepository cardRepository;

    @Autowired
    private Utils utils;

    @Override
    public ResultResponse create(String username, CreateCardRequest request) {

        val user = userRepository.findByUsername(username).get(0);

        val card = new Card();
        card.setUser(user);

        val bookId = request.getBookId();
        val book = bookRepository.findOne(bookId);
        card.setBook(book);
        card.setDate(utils.getNowTimestamp());

        cardRepository.save(card);

        val id = card.getId();
        return ResultResponse.builder()
                .id(id)
                .result(OK_RESULT)
                .build();
    }

    @Override
    public List<CardResponse> getAll(String username, Integer pageNumber, Integer size) {
        val user = userRepository.findByUsername(username).get(0);

        val cards = cardRepository.findByUser(user);

        val skip = size * (pageNumber - 1);
        List<CardResponse> result = cards.stream()
                .skip(skip)
                .limit(size)
                .map((item) -> {
                    CardResponse cardResponse = new CardResponse();
                    Book book = item.getBook();
                    cardResponse.setBookId(book.getId());
                    cardResponse.setCardId(item.getId());
                    return cardResponse;
                })
                .collect(Collectors.toList());
        return result;
    }
}
