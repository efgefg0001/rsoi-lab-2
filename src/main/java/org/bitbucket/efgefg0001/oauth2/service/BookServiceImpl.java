package org.bitbucket.efgefg0001.oauth2.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.val;
import org.bitbucket.efgefg0001.oauth2.Utils;
import org.bitbucket.efgefg0001.oauth2.model.entity.Book;
import org.bitbucket.efgefg0001.oauth2.model.request.BookRequest;
import org.bitbucket.efgefg0001.oauth2.model.request.UpdateBookRequest;
import org.bitbucket.efgefg0001.oauth2.model.response.ResultResponse;
import org.bitbucket.efgefg0001.oauth2.repository.BookRepository;
import org.bitbucket.efgefg0001.oauth2.repository.PublishingHouseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static org.bitbucket.efgefg0001.oauth2.model.response.ResultResponse.OK_RESULT;

@Service
public class BookServiceImpl implements BookService {

    private static final ObjectMapper OM = new ObjectMapper();

    private PublishingHouseRepository publishingHouseRepository;
    private BookRepository bookRepository;
    private Utils utils;

    @Autowired
    public BookServiceImpl(PublishingHouseRepository publishingHouseRepository, BookRepository bookRepository, Utils utils) {
        this.publishingHouseRepository = publishingHouseRepository;
        this.bookRepository = bookRepository;
        this.utils = utils;
    }

    @Override
    public ResultResponse create(BookRequest request, Long publHouseId) {

        val book = utils.copy(request, Book.class);
        val publHouse = publishingHouseRepository.findOne(publHouseId);
        book.setPublishingHouse(publHouse);
        bookRepository.save(book);
        val id = book.getId();

        return ResultResponse.builder()
                .id(id)
                .result(OK_RESULT)
                .build();
    }

    @Override
    @SneakyThrows
    public JsonNode getById(Long id, Long publId) {
        val book = bookRepository.findOne(id);
        val dbId = book.getPublishingHouse().getId();
        if (!dbId.equals(publId))
            throw new Exception("Publishing house with id '" + publId + "' doesn't have book with id = '" + id + "'");
        return OM.convertValue(book, JsonNode.class);
    }

    @Override
    @SneakyThrows
    public ResultResponse deleteById(Long bId, Long pId) {
        val book = bookRepository.findOne(bId);
        val dbId = book.getPublishingHouse().getId();
        if (!dbId.equals(pId))
            throw new Exception("Publishing house with id '" + pId + "' doesn't have book with id = '" + bId + "'");
        bookRepository.delete(bId);
        return ResultResponse.builder()
                .id(bId)
                .result(OK_RESULT)
                .build();
    }

    @Override
    @SneakyThrows
    public JsonNode update(Long bId, Long pId, UpdateBookRequest request) {
        val book = bookRepository.findOne(bId);
        val dbId = book.getPublishingHouse().getId();
        if (!dbId.equals(pId))
            throw new Exception("Publishing house with id '" + pId + "' doesn't have book with id = '" + bId + "'");

        val name = request.getName();
        if (name != null)
            book.setName(name);

        val aut = request.getAuthors();
        if (aut != null)
            book.setAuthors(aut);

        val year = request.getYear();
        if (year != null)
            book.setYear(year);

        val phId = request.getPhId();
        if (phId != null) {
            val ph = publishingHouseRepository.findOne(phId);
            book.setPublishingHouse(ph);
        }

        bookRepository.save(book);

        return OM.convertValue(book, JsonNode.class);
    }
}
