package org.bitbucket.efgefg0001.oauth2.service;


import lombok.val;
import org.bitbucket.efgefg0001.oauth2.Utils;
import org.bitbucket.efgefg0001.oauth2.model.entity.User;
import org.bitbucket.efgefg0001.oauth2.model.entity.UserData;
import org.bitbucket.efgefg0001.oauth2.model.request.SaveUserDataRequest;
import org.bitbucket.efgefg0001.oauth2.model.request.SaveUserRequest;
import org.bitbucket.efgefg0001.oauth2.model.response.UserInfoResponse;
import org.bitbucket.efgefg0001.oauth2.repository.TokenRepository;
import org.bitbucket.efgefg0001.oauth2.repository.UserDataRepository;
import org.bitbucket.efgefg0001.oauth2.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserServiсe {

    private UserRepository userRepository;
    private UserDataRepository userDataRepository;
    private Utils utils;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserDataRepository userDataRepository, Utils utils,
                           TokenRepository tokenRepository) {
        this.userRepository = userRepository;
        this.userDataRepository = userDataRepository;
        this.utils = utils;
    }

    @Override
    @Transactional(readOnly = true)
    public boolean exists(String username) {
        val users = userRepository.findByUsername(username);
        return !users.isEmpty();
    }

    @Override
    @Transactional
    public void save(SaveUserRequest request, SaveUserDataRequest saveUserDataRequest) {
        val user = utils.copy(request, User.class);

        val clientId = utils.createUUID();
        user.setClientId(clientId);
        val clientSecrect = utils.createUUID();
        user.setClientSecret(clientSecrect);
        userRepository.save(user);

        val userData = utils.copy(saveUserDataRequest, UserData.class);
        userData.setUser(user);
        userDataRepository.save(userData);
    }

    @Override
    @Transactional(readOnly = true)
    public User get(String username) {
        val users = userRepository.findByUsername(username);
        val first = users.stream().findFirst();
        return first.get();
    }

    @Override
    @Transactional(readOnly = true)
    public UserInfoResponse getUserInfo(String username) {
        val user = this.get(username);
        val userData = userDataRepository.findByUser(user);
        return utils.copy(userData, UserInfoResponse.class);
    }
}
