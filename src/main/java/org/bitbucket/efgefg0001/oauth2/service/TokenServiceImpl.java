package org.bitbucket.efgefg0001.oauth2.service;

import lombok.SneakyThrows;
import lombok.val;
import org.bitbucket.efgefg0001.oauth2.Utils;
import org.bitbucket.efgefg0001.oauth2.model.entity.Token;
import org.bitbucket.efgefg0001.oauth2.model.enums.TokenType;
import org.bitbucket.efgefg0001.oauth2.model.response.TokenResponse;
import org.bitbucket.efgefg0001.oauth2.repository.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Calendar;

@Service
public class TokenServiceImpl implements TokenService {

    private TokenRepository tokenRepository;
    private Utils utils;

    @Autowired
    public TokenServiceImpl(TokenRepository tokenRepository, Utils utils) {
        this.tokenRepository = tokenRepository;
        this.utils = utils;
    }

    @Override
    @Transactional
    public TokenResponse createToken(String username, Long expiresIn) {

        val token = new Token();
        token.setUsername(username);
        val accessToken = utils.createUUID();
        token.setAccessToken(accessToken);
        val now = utils.getNowTimestamp();
        token.setBegin(now);
        token.setExpiresIn(expiresIn);
        val refreshToken = utils.createUUID();
        token.setRefreshToken(refreshToken);
        token.setTokenType(TokenType.BEARER.getValue());

        tokenRepository.save(token);

        return utils.copy(token, TokenResponse.class);
    }

    @Override
    @Transactional(readOnly = true)
    public boolean tokenExists(String accessToken) {
        val tokens = tokenRepository.findByAccessToken(accessToken);
        if (tokens != null && tokens.size() > 0)
            return true;
        return false;
    }

    @Override
    public boolean tokenExpired(String accessToken) {
        val tokens = tokenRepository.findByAccessToken(accessToken);
        val token = tokens.get(0);

        val begin = token.getBegin();
        val cal = Calendar.getInstance();
        cal.setTimeInMillis(begin.getTime());
        val delta = token.getExpiresIn().intValue();
        cal.add(Calendar.SECOND, delta);
        val end = new Timestamp(cal.getTime().getTime());

        val now = utils.getNowTimestamp();

        if (end.compareTo(now) > 0)
            return false;
        else
            return true;
    }

    @Override
    @Transactional(readOnly = true)
    @SneakyThrows
    public String getUsernameByAccessToken(String accessToken) {
        val tokens = tokenRepository.findByAccessToken(accessToken);
        if (tokens == null || tokens.size() == 0)
            throw new Exception("refresh_token is invalid");
        val token = tokens.get(0);
        return token.getUsername();
    }

    @Override
    @Transactional(readOnly = true)
    public String getUsernameByRefreshToken(String refreshToken) {
        val tokens = tokenRepository.findByRefreshToken(refreshToken);
        val token = tokens.get(0);
        return token.getUsername();
    }
}
