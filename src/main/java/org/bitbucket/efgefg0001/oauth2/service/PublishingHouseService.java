package org.bitbucket.efgefg0001.oauth2.service;

import com.fasterxml.jackson.databind.JsonNode;
import org.bitbucket.efgefg0001.oauth2.model.request.PublHouseRequest;
import org.bitbucket.efgefg0001.oauth2.model.response.ResultResponse;

import java.util.List;

public interface PublishingHouseService {
    ResultResponse create(PublHouseRequest request);
    JsonNode getById(Long id);
    JsonNode update(Long id, PublHouseRequest data);
    List<JsonNode> getWithPaging(Integer pageNumber, Integer size);
}
