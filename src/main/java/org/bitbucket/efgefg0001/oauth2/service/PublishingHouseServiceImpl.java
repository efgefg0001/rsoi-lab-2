package org.bitbucket.efgefg0001.oauth2.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import org.bitbucket.efgefg0001.oauth2.Utils;
import org.bitbucket.efgefg0001.oauth2.model.entity.PublishingHouse;
import org.bitbucket.efgefg0001.oauth2.model.request.PublHouseRequest;
import org.bitbucket.efgefg0001.oauth2.model.response.ResultResponse;
import org.bitbucket.efgefg0001.oauth2.repository.PublishingHouseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.bitbucket.efgefg0001.oauth2.model.response.ResultResponse.OK_RESULT;

@Service
public class PublishingHouseServiceImpl implements PublishingHouseService {

    private static final ObjectMapper OM = new ObjectMapper();

    private PublishingHouseRepository publishingHouseRepository;
    private Utils utils;

    @Autowired
    public PublishingHouseServiceImpl(PublishingHouseRepository publishingHouseRepository, Utils utils) {
        this.publishingHouseRepository = publishingHouseRepository;
        this.utils = utils;
    }


    @Override
    public ResultResponse create(PublHouseRequest request) {
        val publHouse = utils.copy(request, PublishingHouse.class);
        publishingHouseRepository.save(publHouse);
        val id = publHouse.getId();
        return ResultResponse.builder()
                .id(id)
                .result(OK_RESULT)
                .build();
    }

    @Override
    public JsonNode getById(Long id) {
        val publHouse = publishingHouseRepository.findOne(id);
        return OM.convertValue(publHouse, JsonNode.class);
    }

    @Override
    public JsonNode update(Long id, PublHouseRequest data) {
        val publHouse = publishingHouseRepository.findOne(id);

        val city = data.getCity();
        if (city != null)
            publHouse.setCity(city);

        val country = data.getCountry();
        if (country != null)
            publHouse.setCountry(country);

        val name = data.getName();
        if (name != null)
            publHouse.setName(name);

        val year = data.getYear();
        if (year != null)
            publHouse.setYear(year);

        publishingHouseRepository.save(publHouse);

        return OM.convertValue(publHouse, JsonNode.class);
    }

    @Override
    public List<JsonNode> getWithPaging(Integer pageNumber, Integer size) {
        val iterable = publishingHouseRepository.findAll();
        val list = new ArrayList<PublishingHouse>();
        iterable.forEach(list::add);
        val skip = size * (pageNumber - 1);
        List<JsonNode> result = list.stream()
                .skip(skip)
                .limit(size)
                .map((item) -> OM.convertValue(item, JsonNode.class))
                .collect(Collectors.toList());
        return result;
    }
}
