package org.bitbucket.efgefg0001.oauth2.service;

import org.bitbucket.efgefg0001.oauth2.model.request.CreateCardRequest;
import org.bitbucket.efgefg0001.oauth2.model.response.CardResponse;
import org.bitbucket.efgefg0001.oauth2.model.response.ResultResponse;

import java.util.List;

public interface CardService {
    ResultResponse create(String username, CreateCardRequest request);

    List<CardResponse> getAll(String username, Integer pageNumber, Integer size);
}
