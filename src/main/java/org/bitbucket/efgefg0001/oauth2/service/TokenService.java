package org.bitbucket.efgefg0001.oauth2.service;

import org.bitbucket.efgefg0001.oauth2.model.response.TokenResponse;

public interface TokenService {
    TokenResponse createToken(String username, Long expiresIn);
    boolean tokenExists(String accessToken);
    boolean tokenExpired(String accessToken);
    String getUsernameByAccessToken(String accessToken);
    String getUsernameByRefreshToken(String refreshToken);
}
