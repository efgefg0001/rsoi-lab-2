package org.bitbucket.efgefg0001.oauth2.service;

import org.bitbucket.efgefg0001.oauth2.model.entity.User;
import org.bitbucket.efgefg0001.oauth2.model.request.SaveUserDataRequest;
import org.bitbucket.efgefg0001.oauth2.model.request.SaveUserRequest;
import org.bitbucket.efgefg0001.oauth2.model.response.UserInfoResponse;

public interface UserServiсe {
    boolean exists(String username);
    void save(SaveUserRequest user, SaveUserDataRequest userData);
    User get(String username);
    UserInfoResponse getUserInfo(String username);
}
